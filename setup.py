# -*- coding:utf-8 -*-
"""A setuptools based setup module for the maild service.
"""

import setuptools

setuptools.setup(
    name="servicectl",
    version="0.1.0",
    description="Python process demonization and control.",
    url="https://gitlab.com/vbogretsov/servicectl",
    author="vbogretsov",
    author_email="bogrecov@gmail.com",
    license="BSD",
    py_modules=["servicectl"],
    install_requires=["fasteners", "setproctitle"])
