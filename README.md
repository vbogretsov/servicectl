# servicectl 0.1.0

## Installation

````bash
pip install -e git+https://gitlab.com/vbogretsov/servicectl.git#egg=servicectl-0.1.0
````

## Usage

````python
# samplectl.py
import asyncio
import logging
import logging.config
import os
import signal
import time

import click
import servicectl


LOG_CONF = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {
            "class": "logging.Formatter",
            "format": "%(asctime)s %(name)s [%(levelname)s]: %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S"
        }
    },
    "handlers": {
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "standard",
            "level": "DEBUG",
            "filename": "/tmp/sampled.log",
            "maxBytes": 1048576,
            "backupCount": 5
        }
    },
    "loggers": {
        "": {
            "handlers": ["file"],
            "level": "DEBUG",
            "propagate": True
        }
    }
}


class SamepleService(servicectl.Service):

    def run(self):
        logging.config.dictConfig(LOG_CONF)
        LOG = logging.getLogger(__name__)

        LOG.info("Daemon started with pid %s\n", os.getpid())

        async def run():
            while True:
                LOG.info("Daemon Alive! %s\n", time.ctime())
                await asyncio.sleep(1)

        loop = asyncio.get_event_loop()

        def stop():
            LOG.info("Exit daemon")
            loop.stop()

        loop.add_signal_handler(signal.SIGTERM, stop)
        loop.run_until_complete(run())


@click.group()
def ctl():
    """Service control for maild.
    """


@ctl.command()
def start():
    service = SamepleService("sampled", pid_dir="/tmp")
    service.start()


@ctl.command()
def stop():
    service = SamepleService("sampled", pid_dir="/tmp")
    service.stop()


@ctl.command()
def status():
    service = SamepleService("sampled", pid_dir="/tmp")
    if service.is_running():
        print("running")
    else:
        print("not running")


if __name__ == '__main__':
    ctl()

````

## License

See the license file