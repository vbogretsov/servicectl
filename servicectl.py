# -*- coding:utf-8 -*-
"""Python process demonization and control.
"""
import atexit
import errno
import os
import resource
import signal
import sys

import fasteners
import setproctitle

MAXFD = 2048


def get_maximum_file_descriptors():
    """Get the maximum number of open file descriptors for this process.

    Returns:
        int : The number to use as the maximum number of open files for this
            process. The maximum is the process hard resource limit of maximum
            number of open file descriptors. If the limit is “infinity”, a
            default value of ``MAXFD`` is returned.
    """
    _, limit = resource.getrlimit(resource.RLIMIT_NOFILE)
    return limit if limit != resource.RLIM_INFINITY else MAXFD


def close_file_descriptor_if_open(fd):
    """Close a file descriptor if already open.

    Close the file descriptor `fd`, suppressing an error in the case the file
    was not open.

    Args:
        fd (int): The file descriptor to close.
    """
    try:
        os.close(fd)
    except EnvironmentError as exc:
        if exc.errno == errno.EBADF:
            # File descriptor was not open.
            pass
        else:
            raise


def close_all_open_files(exclude=()):
    """Closes every file descriptor (if open) of this process.

    Args:
        exclude (iterable): Collection of the file descriptors to skip when
            closing files.
    """
    maxfd = get_maximum_file_descriptors()
    for fd in reversed(range(maxfd)):
        if fd not in exclude:
            close_file_descriptor_if_open(fd)


def daemonize(entrypoint,
              name,
              pidfile,
              stdin="/dev/null",
              stdout="/dev/null",
              stderr="/dev/null"):
    """Run the function provided in the daemon process.

    Args:
        entrypoint (callable): Daemon entry point.
        name (str): The daemon name desired.
        pidfile (str): The full PID file path.
        stdin (str): Daemon's STDIN.
        stdout (str): Daemon's STDOUT.
        stdin (str): Daemon's STDERR.

    Returns:
        bool: ``True`` if daemon launched otherwise ``False``.

    Raises:
        RuntimeError: If the system call `fork` failed.
    """

    if os.path.exists(pidfile):
        return False

    lock = fasteners.InterProcessLock(pidfile)
    if not lock.acquire(blocking=False):
        return False

    try:
        if os.fork() > 0:
            lock.release()
            return True
    except OSError as e:
        lock.release()
        raise RuntimeError("fork #1 failed.")

    os.chdir("/")
    os.umask(0)
    os.setsid()

    setproctitle.setproctitle(name)

    try:
        if os.fork() > 0:
            raise SystemExit(0)
    except OSError as e:
        raise RuntimeError("fork #2 failed.")

    sys.stdout.flush()
    sys.stderr.flush()

    close_all_open_files()

    with open(stdin, "rb", 0) as f:
        os.dup2(f.fileno(), sys.stdin.fileno())
    with open(stdout, "ab", 0) as f:
        os.dup2(f.fileno(), sys.stdout.fileno())
    with open(stderr, "ab", 0) as f:
        os.dup2(f.fileno(), sys.stderr.fileno())

    # Write the PID file
    with open(pidfile, "w") as f:
        print(os.getpid(), file=f)

    # Arrange to have the PID file removed on exit/signal
    atexit.register(lambda: os.remove(pidfile))

    # Signal handler for termination (required)
    def sigterm_handler(signo, frame):
        raise SystemExit(1)

    signal.signal(signal.SIGTERM, sigterm_handler)

    entrypoint()


class Service:
    """A background service.

    This class provides the basic framework for running and controlling
    a background daemon. This includes methods for starting the daemon
    (including things like proper setup of a detached deamon process),
    checking whether the daemon is running, asking the daemon to
    terminate and for killing the daemon should that become necessary.
    """

    def __init__(self, name, pid_dir="/var/run", **kwargs):
        """ Constructor.

        name (str): A string that identifies the daemon. The name is used for
            the name of the daemon process and the PID file.
        pid_dir (str): The directory in which the PID file is stored.
        """
        self.name = name
        self.pid_file = os.path.join(pid_dir, "{0}.pid".format(name))

    @property
    def pid(self):
        if os.path.exists(self.pid_file):
            with open(self.pid_file, "r") as file:
                return int(file.read())
        return None

    def start(self):
        """Start the daemon process.

        The daemon process is started in the background and the calling
        process returns.

        Returns:
            bool: ``True`` if daemon started otherwise ``False``.

        Raises:
            PermissionError: If no permissions to create the PID file.
            RuntimeError: If the system call `fork` failed.
        """
        if self.pid:
            return False
        return daemonize(self.run, self.name, self.pid_file)

    def stop(self):
        """Tell the daemon process to stop.

        Sends the SIGTERM signal to the daemon process, requesting it
        to terminate.
        """
        if not self.pid:
            return False
        os.kill(self.pid, signal.SIGTERM)
        return True

    def is_running(self):
        """Check if the daemon is running.

        Returns:
            bool: ``True`` if the daemon is running otherwise ``False``.
        """
        return self.pid is not None

    def run(self):
        """The daemon entry point.

        This method should be overriden in the derived class.
        """
        pass
